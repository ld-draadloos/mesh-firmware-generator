#!/bin/bash
set -ue

function download {
  ROOT=$(pwd)
  if [ ! -e ${ROOT}/image_builders/$1 ]; then
    mkdir -p ${ROOT}/dl
    cd ${ROOT}/dl
    [ -e $(basename $2) ] || wget $2
    mkdir -p ${ROOT}/image_builders/$1
    cd ${ROOT}/image_builders/$1
    tar xvf ../../../dl/$(basename $2)
    cd ${ROOT}
  fi 
}

download ar71xx/generic https://downloads.openwrt.org/chaos_calmer/15.05/ar71xx/generic/OpenWrt-ImageBuilder-15.05-ar71xx-generic.Linux-x86_64.tar.bz2
download brcm2708/bcm2708 https://downloads.openwrt.org/chaos_calmer/15.05/brcm2708/bcm2708/OpenWrt-ImageBuilder-15.05-brcm2708-bcm2708.Linux-x86_64.tar.bz2
download brcm2708/bcm2709 https://downloads.openwrt.org/chaos_calmer/15.05/brcm2708/bcm2709/OpenWrt-ImageBuilder-15.05-brcm2708-bcm2709.Linux-x86_64.tar.bz2
download x86/generic https://downloads.openwrt.org/chaos_calmer/15.05/x86/generic/OpenWrt-ImageBuilder-15.05-x86-generic.Linux-x86_64.tar.bz2

#download ar71xx/generic https://downloads.lede-project.org/snapshots/targets/ar71xx/generic/LEDE-ImageBuilder-ar71xx-generic.Linux-x86_64.tar.bz2
#download brcm2708/bcm2708 https://downloads.lede-project.org/snapshots/targets/brcm2708/bcm2708/LEDE-ImageBuilder-brcm2708-bcm2708.Linux-x86_64.tar.bz2
#download brcm2708/bcm2709 https://downloads.lede-project.org/snapshots/targets/brcm2708/bcm2709/LEDE-ImageBuilder-brcm2708-bcm2709.Linux-x86_64.tar.bz2
#download x86/generic https://downloads.lede-project.org/snapshots/targets/x86/generic/LEDE-ImageBuilder-x86-generic.Linux-x86_64.tar.bz2

#download ar71xx/generic "https://downloads.openwrt.org/chaos_calmer/15.05.1/ar71xx/generic/OpenWrt-ImageBuilder-15.05.1-ar71xx-generic.Linux-x86_64.tar.bz2"
#download brcm2708/bcm2708 "https://downloads.openwrt.org/chaos_calmer/15.05.1/brcm2708/bcm2708/OpenWrt-ImageBuilder-15.05.1-brcm2708-bcm2708.Linux-x86_64.tar.bz2"
#download brcm2708/bcm2709 "https://downloads.openwrt.org/chaos_calmer/15.05.1/brcm2708/bcm2709/OpenWrt-ImageBuilder-15.05.1-brcm2708-bcm2709.Linux-x86_64.tar.bz2"
#download x86/generic "https://downloads.openwrt.org/chaos_calmer/15.05.1/x86/generic/OpenWrt-ImageBuilder-15.05.1-x86-generic.Linux-x86_64.tar.bz2"

