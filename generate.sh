#!/bin/bash
set -ue

CONFIGS_DIR="../mesh-router-configs/"
TEMPLATE_DIR="../mesh-router-files/"
PACKAGES_BASE="babeld fastd ip -odhcpd -ppp -ppp-mod-pppoe"
PACKAGES_DNS="unbound"

ROOT=$PWD
rm -fr generated-config
mkdir -p generated-config
find $CONFIGS_DIR -type f -iname "*.json" -print0 | while IFS= read -r -d $'\0' line; do
  echo -----------------------------------
  echo $line
  echo -----------------------------------
  CONFIG=$line

  CONFIG_BASENAME=`basename $CONFIG`
  CONFIG_FILENAME="${CONFIG_BASENAME%.*}"
  OUT_DIR="$(pwd)/bin/$CONFIG_FILENAME"
  SUBARCH=`jshon -e sub_arch -u < $CONFIG 2> /dev/null || echo "generic"`
  ARCH=`jshon -e arch -u < $CONFIG`
  PROFILE=`jshon -e profile -u < $CONFIG 2> /dev/null || true`
  HWVERSION=`jshon -e hw_version -u < $CONFIG 2> /dev/null || true`
  PACKAGES_DEVICE="$PACKAGES_BASE `jshon -e packages -u < $CONFIG 2> /dev/null || true`"
  if [ "`jshon -e dns_enabled -u < $CONFIG 2> /dev/null || true`" = true ] ; then
    PACKAGES_DEVICE="$PACKAGES_DEVICE $PACKAGES_DNS"
  fi
  mkdir -p generated-config/${CONFIG_FILENAME}  
  cp -r $TEMPLATE_DIR/* generated-config/${CONFIG_FILENAME}
  cp $CONFIG generated-config/${CONFIG_FILENAME}/etc/device-settings.json

  #------------------
  cd image_builders/$ARCH/$SUBARCH
  cd *
  rm -rf bin/*
  rm -f $OUT_DIR/*
  make image PROFILE="$PROFILE" PACKAGES="$PACKAGES_DEVICE" FILES="${ROOT}/generated-config/${CONFIG_FILENAME}" BIN_DIR="$OUT_DIR"
  cd $ROOT

  if [ -n "$HWVERSION" ]
  then
    mkdir -p $OUT_DIR
    cd $OUT_DIR
    find . -type f -iname "*" -print0 | grep --null-data --invert-match -E "$HWVERSION|sums" | while IFS= read -r -d $'\0' file; do
      rm $file
    done
    cd $ROOT
  fi

done
